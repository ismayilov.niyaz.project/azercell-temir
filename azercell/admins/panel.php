<?php 
	require_once '../init/core.php';
	include '../_helper.php';
	include 'partials/_head.php';
	include 'partials/_header.php';

	if (isset($_SESSION['role'])) {
		$role=$_SESSION['role'];
	}

	else
		header('location:index.php');
?>

<?php 
	if ($role==1) {
		include 'partials/_superAdmin_panel.php';
	}

	if ($role==2) {
		include 'partials/_company_panel.php';
	}

	if ($role==3) {
		include 'partials/_repairer_panel.php';

	}
?>

<?php include 'partials/_footer.php' ?>