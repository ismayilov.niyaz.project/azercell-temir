 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    

    <script>
    	$(document).ready(function(){
    		$('.register-container').css({
    			'height':$('body').height()-$('header').height()
    		});

    		$('.register_toggle').click(function(){
    			$('.register').css({
    				'display':'block'
    			});
    			$('.login').css({
    				'display':'none'
    			});
    		});

    		$('.login_toggle').click(function(){
    			$('.register').css({
    				'display':'none'
    			});
    			$('.login').css({
    				'display':'block'
    			});
    		});
    	});
    </script>
    <script>
    $(document).ready(function(){
        $('.panel-container').css({
                'height':$('body').height()-$('header').height()
            });

        $('.status').change(function(){
            var status=$(this).val();
            var id=$(this).attr('id');
            $.ajax({
                method:'post',
                url:'partials/_changeStatus.php',
                data:{'status':status,'id':id},
                success:function(data){
                    $('#'+id).html(data);
                },
                error:function(){alert('something went wrong')}
            });
        });

        $('.diaqnoz').click(function(){
            $('.tables').css({
                'display':'none'
            })

            $('.diaqnoz_qoyulmali').css({
                'display':'block'
            })
        });

        $('.temir').click(function(){
            $('.tables').css({
                'display':'none'
            })

            $('.temir_edilmeli').css({
                'display':'block'
            })
        });

        $('.legv').click(function(){
            $('.tables').css({
                'display':'none'
            })

            $('.legv_edilenler').css({
                'display':'block'
            })
        });

     
    });
</script>
  </body>
</html>