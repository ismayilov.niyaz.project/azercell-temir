<?php 
	if (isset($_SESSION['username'])) {
		$username=$_SESSION['username'];
		//all orders
		$order_query=mysqli_query($conn,"select * from orders");
		//get queue
		date_default_timezone_set("Asia/Baku");
		$date=date("Y-m-d");
		$queue=mysqli_query($conn,"SELECT * FROM `queue` WHERE `queue_status_id`=1 or `queue_status_id`=2 and `date`='$date'  order by `time` asc");
		$queue_gecikmis=mysqli_query($conn,"SELECT * FROM `queue` WHERE `queue_status_id`=4 and `date`='$date'  order by `time` asc");

		if (isset($_GET['q']) && $_GET['q']==1) {
			if (isset($_POST['add_company'])) {
				if (!isset($_POST['token']) || !verifyToken($_POST['token'])) {
		      echo "invalid CSRF! ";die();
		    }
		    else
		    {



				$username=stripper($_POST['username']);
				$password=stripper($_POST['password']);
				$company_id=stripper($c_id);
				$role=2;
				$password=password_hash($password,PASSWORD_DEFAULT);
				$add=mysqli_query($conn,"insert into users(username,password,role) values('$username','$password','$role')");
				header('location:panel.php');}
			}
		}

		if (isset($_GET['q']) && $_GET['q']==2) {
			if (isset($_POST['add_order'])) {
				if (!isset($_POST['token']) || !verifyToken($_POST['token'])) {
      echo "invalid CSRF! ";die();
    }
    else{

    
				$name=stripper($_POST['name']);
	  			$surname=stripper($_POST['surname']);
	  			$father_name=stripper($_POST['father_name']);
	  			$seria_no=stripper($_POST['seria_no']);
	  			$fin=stripper($_POST['fin']);
	  			$phone=stripper($_POST['phone']);
	  			$email=stripper($_POST['email']);
	  			$device_name=stripper($_POST['device_name']);
	  			$add=mysqli_query($conn,"insert into orders(name,surname,father_name,seria_no,fin,phone,status,email,device_name) values('$name','$surname','$father_name','$seria_no','$fin','$phone','1','$email','$device_name')");
	  			header('location:panel.php');}

			}
		}

		if (isset($_GET['q']) && $_GET['q']==3) {

			if (isset($_POST['qebul_et'])) {
				$id=$_POST['id'];
				//echo "$id";die();
				$u= mysqli_query($conn,"UPDATE queue set `queue_status_id`=2 WHERE `id`='$id'");
				if (!$u) {
					echo mysqi_error($conn);die();
				}

				header('location:panel.php?q=3');

			}

			if (isset($_POST['bitirdim'])) {
				$id=$_POST['id'];
				mysqli_query($conn,"UPDATE queue set `queue_status_id`=3 WHERE `id`='$id'");
				header('location:panel.php?q=3');

			}

			if (isset($_POST['novbeti'])) {
				$id=$_POST['id'];
				mysqli_query($conn,"UPDATE queue set `queue_status_id`=4 WHERE `id`='$id'");
				header('location:panel.php?q=3');

			}

			if (isset($_POST['cancel'])) {
				$id=$_POST['id'];
				//echo "$id";die();
				mysqli_query($conn,"UPDATE queue set `queue_status_id`=3 WHERE `id`='$id'");
				header('location:panel.php?q=3');

			}

			if (isset($_POST['call'])) {
				$id=$_POST['id'];
				//echo "$id";die();
				mysqli_query($conn,"UPDATE queue set `queue_status_id`=1 WHERE `id`='$id'");
				header('location:panel.php?q=3');
			}

		}
	}

	else
		header('location:index.php');



?>

<div class="container-fluid panel-container" >
	<div class="row no-gutters" style="margin-right: -15px;
  margin-left: -15px;height: 100%">
		<div class="col-12">
			<?php if(!isset($_GET['q'])): ?>
				<div class="text-center">
				<a href="panel.php?q=1" class="btn btn-primary my-2 mx-2" >Şirkət Əlavə Et</a>
				<a href="panel.php?q=2" class="btn btn-primary my-2 mx-2" >Sifariş Əlavə Et</a>
				<a href="panel.php?q=3" class="btn btn-primary my-2 mx-2" >Növbə</a>
				</div>
				<div class="row justify-content-center">
					<div class="col-11">
						<!-- TABLE -->
						<h2 class="text-center mt-3">Sifarisler</h2>
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Ad</th>
									<th>Soyad</th>
									<th>Seria Nomresi</th>
									<th>FIN</th>
									<th>Email</th>
									<th>Telefon Markasi</th>
									<th>Sirket</th>
									<th>Status</th>
								</tr>
							</thead>
	<tbody>
		<?php while($row=mysqli_fetch_array($order_query)): ?>
			<?php 
				//all companies
		$company_query=mysqli_query($conn,"select * from users where role='2'");
			?>
			<tr>
				<td></td>
				<td><?=$row['name']?></td>
				<td><?=$row['surname']?></td>
				<td><?=$row['seria_no']?></td>
				<td><?=$row['fin']?></td>
				<td><?=$row['email']?></td>
				<td><?=$row['device_name']?></td>
				<td>
					<select name="company_name" class="form-control company_name" id="<?=$row['id']?>">
						<option value="" <?=$row['users_id']==0?" selected='selected'":''?>></option>
				<?php while($row2=mysqli_fetch_array($company_query)): ?>
					<option value="<?=$row2['id']?>" <?=$row['users_id']==$row2['id']?" selected='selected'":''?>><?=$row2['username']?></option>
				<?php endwhile; ?>
				</select>
				</td>
				<td>
					<select name="status" class="form-control status1" id="<?=$row['id']?>">
						<option value="1" <?=$row['status']==1?" selected='selected'":''?>
							>Temir Merkezindedir</option>
						<option value="0" <?=$row['status']==0?" selected='selected'":''?>>Ofisdedir</option>
					</select>
				</td>
			</tr>
		<?php endwhile ?>
	</tbody>

						</table>
					</div>
				</div>
			<?php endif ?>
			<?php if(isset($_SESSION['success'])): ?>
				<div class="row justify-content-center">
					<div class="col-10">
						<div class="alert alert-success">
						<?php 
							echo $_SESSION['success']; 
							unset($_SESSION['success']); 
						?>
						</div>
					</div>
				</div>
			<?php endif ?>
			<?php if(isset($_GET['q']) && $_GET['q']==1): ?>
				<div class="row justify-content-center">
					<div class="col-8 mr-3">
					
				<form action="panel.php?q=1" method="post">
					<div class="form-group">
						<label for="username">Username:</label>
						<input required type="text" name="username" class="form-control">
						<label for="password">Password:</label>
						<input required type="password" class="form-control" name="password">
						<input type="hidden" class='token' name="token" value="<?=$_SESSION['token']?>">
					</div>
					<button type="submit" name="add_company" class="btn btn-success float-right">Əlavə Et</button>
					<a href="panel.php" class="btn btn-default float-right">Ləğv Et</a>
					
				</form>

					</div>
				</div>
			<?php endif ?>
			<?php if(isset($_GET['q']) && $_GET['q']==2): ?>
				<div class="row justify-content-center">
					<div class="col-8 mr-3">
					
				<form action="panel.php?q=2" method="post">
					<div class="form-group">
						<label for="name">Ad:</label>
						<input required type="text" class="form-control" id="name" name="name">

						<label for="surname">Soyad:</label>
						<input required type="text" class="form-control" id="surname" name="surname">

						<label for="father_name">Atasının Adı:</label>
						<input required type="text" class="form-control" id="father_name" name="father_name">

						
					<div class="row">
						<div class="col-md-6">
							<label for="seria_no">Seriya Nömrəsi:</label>
						<input required type="number" class="form-control" id="seria_no" name="seria_no">
						</div>
						<div class="col-md-6">
							<label for="fin">Fin:</label>
						<input required type="text" class="form-control" id="fin" name="fin">
						</div>
					</div>
						
						
						<label for="phone">Telefon:</label>
						<input required type="text" class="form-control" id="phone" name="phone">
					<div class="row">
						<div class="col-md-6">
							<label for="email">Email:</label>
						<input required type="email" class="form-control" id="email" name="email">
						</div>
						<div class="col-md-6">
							<label for="device_name">Telefon Markasi:</label>
						<input required type="text" class="form-control" id="device_name" name="device_name">
						</div>
						<input type="hidden" class='token' name="token" value="<?=$_SESSION['token']?>">
					</div>
						
					</div>
					<button type="submit" name="add_order" class="btn btn-success float-right">Əlavə Et</button>
					<a href="panel.php" class="btn btn-default float-right">Ləğv Et</a>
					
				</form>

					</div>
				</div>
			<?php endif ?>
<?php if(isset($_GET['q']) && $_GET['q']==3): ?>
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="row justify-content-center">
				<div class="col-md-5">
					<div class="jumbotron jumbotron-fluid">
					  <h1 class="display-5 text-center">
					  	<?php 
					  	$row=mysqli_fetch_array($queue);
					  	echo $row['ticket_number'];
					  	?>
					  </h1>
					  <?php if($row['queue_status_id']==2): ?>
					  <p class="text-danger lead text-center"><strong>Xidmet Gosterilir</strong></p>
					<?php endif ?>
					</div>
					<form action="" method="post" class="d-inline">
						<input type="hidden" name="id" value="<?=$row['id']?>">
						<button type="submit" class="btn btn-primary" name="bitirdim">Bitirdim</button>
					</form>
					<form action="" method="post" class="d-inline">
						<input type="hidden" name="id" value="<?=$row['id']?>">
						<button type="submit" class="btn btn-primary" name="qebul_et">Qebul Et</button>
					</form>
					<form action="" method="post"  class="d-inline">
						<input type="hidden" name="id" value="<?=$row['id']?>">
						<button type="submit" class="btn btn-primary" name="novbeti">Gecikib</button>
					</form>
				</div>
			</div>
			<table class="table mt-1">
				<thead>
					<tr>
						<th>#</th>
						<th>Seria Nomresi</th>
						<th>Bilet Nomresi</th>
						<th>Vaxt</th>
					</tr>
				</thead>
				<tbody>
					<?php while($row=mysqli_fetch_array($queue_gecikmis)): ?>
						<?php 
							$q=mysqli_query($conn,"select * from orders where id='$row[order_id]'");
							$r=mysqli_fetch_array($q);

						?>
						<tr>
							<td>
								<form action="" class="d-inline" method="post">
									<input type="hidden" name='id' value="<?=$row['id']?>" >
									<button type="submit" name="cancel" class="btn btn-sm btn-danger">LEGV ET</button>
								</form>
								<form action="" method="post" class="d-inline">
									<input type="hidden" name="id" value="<?=$row['id']?>">
									<button name="call" type="submit" class="btn btn-sm btn-primary">CAGIR</button>
								</form>
							</td>
							<td><?=$r['seria_no']?></td>
							<td><?=$row['ticket_number']?></td>
							<td><?=$row['time']?></td>
						</tr>
					<?php endwhile ?>
				</tbody>
			</table>
			
				
		</div>
	</div>
	
<?php endif ?>
		</div>
		<!-- <div class="col-5" style="height: 100%">
			<img src="../images/Coventry-Mobile-Phone-Repairs-1.png" alt="" class="img-fluid Coventry-Mobile-Phone-Repairs">
		</div> -->
	</div>
</div>

<script>
	$(document).ready(function(){
		$('.panel-container').css({
    			'height':$('body').height()-$('header').height()
    		});

		$('.company_name').change(function(){
			var company=$(this).val();
			var id=$(this).attr('id');
			//alert('asd')
			$.ajax({
				method:'post',
				url:'partials/_admin_change_company.php',
				data:{'company':company,'id':id,},
				success:function(data){
					$(".company_name #"+id).html(data);	
				},
				error:function(){alert('something went wrong')}
			});
		});

		$('.status1').change(function(){
			
			var status=$(this
				).val();
			var id=$(this).attr('id');
			
			$.ajax({
				method:'post',
				url:'partials/_admin_change_status.php',
				data:{'status':status,'id':id},
				success:function(data){
					$(".status1 #"+id).html(data);
				},
				error:function(){alert('something went wrong')}
			});
		});
	});
</script>
<?php include 'partials/_footer.php'; ?>