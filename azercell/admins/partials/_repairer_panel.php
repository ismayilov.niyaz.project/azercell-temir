<?php 
	// require_once '../../init/core.php';
	if (isset($_SESSION['username'])){
		$username=$_SESSION['username'];	
		$user_query=mysqli_query($conn,"Select * from users where username='$username'");
		$user_info=mysqli_fetch_array($user_query);
		$user_id=$user_info['id'];

		//get repair id
		$repair_query=mysqli_query($conn,"select * from repair where user_id='$user_id'");
		$repair_info=mysqli_fetch_array($repair_query);
		$repair_id=$repair_info['id'];

		//get orders of repair
		$agree=mysqli_query($conn,"select * from orders where repair_id='$repair_id' and repair_status='1' ");
		$disagree=mysqli_query($conn,"select * from orders where repair_id='$repair_id' and repair_status='2'");
		$diaqnoz=mysqli_query($conn,"select * from orders where repair_id='$repair_id' and repair_status='0'");

		if (isset($_POST['submit'])) {
			if (!isset($_POST['token']) || !verifyToken($_POST['token'])) {
		      echo "invalid CSRF! ";die();
		    }
		    else
		    {


			$d=stripper($_POST['diagnose']);
			$p=stripper($_POST['price']);
			$id=stripper($_POST['order_id']);

			mysqli_query($conn,"insert into diagnose(name,order_id) values('$d','$id')");
			
			mysqli_query($conn,"update orders set price='$p' where id='$id'");
			header('location:panel.php');}
		}

		if (isset($_POST['update'])) {
			if (!isset($_POST['token']) || !verifyToken($_POST['token'])) {
		      echo "invalid CSRF! ";die();
		    }
		    else
		    {


			$d=stripper($_POST['diagnose']);
			$p=stripper($_POST['price']);
			$id=stripper($_POST['order_id']);

			mysqli_query($conn,"update diagnose set name='$d' where id='$id'");
			
			mysqli_query($conn,"update orders set price='$p' where id='$id'");
			header('location:panel.php');}
		}

		if (isset($_GET['q'] )&& $_GET['q']==1) {
			if (isset($_POST['add_new_status'])) {
				if (!isset($_POST['token']) || !verifyToken($_POST['token'])) {
			      echo "invalid CSRF! ";die();
			    }
			    else
			    {


				$new_status=stripper($_POST['new_status']);
				$order_id=stripper($_POST['order_id']);
				mysqli_query($conn,"insert status(status_name,order_id,repair_id) values('$new_status','$order_id','$repair_id')");
				$id=mysqli_insert_id($conn);
				$k=mysqli_query($conn,"update orders set status_id='$id' where id='$order_id'");
				// if (!$k) {
				// 	echo mysqli_error($conn);die();
				// }
				header('location:panel.php');}
			}
		}
	}
	else
	{
		header('location:index.php');
	}
?>

<div class="container-fluid panel-container" >
	<div class="row no-gutters" style="margin-right: -15px;
  margin-left: -15px;height: 100%">
		<div class="col-7">

				<div class="row justify-content-center">
					<div class="col-11">
<div class="row justify-content-center mt-5" >
	<div class="col-md-10">
		<button class="btn btn-primary diaqnoz">Diaqnoz qoymalilar</button>
		<button class="btn btn-primary temir">Temir edilmeliler</button>
		<button class="btn btn-primary legv">Legv edilenler</button>
		
	</div>
</div>
<div class="temir_edilmeli tables" style="display: block;">
	<table class="table" >
		<h4 class="text-center my-3 ">Temir elemeli</h4>
		<thead>
			<tr><th>Ad</th><th>Soyad</th><th>Atasının Adı</th><th>Telefon</th><th>status</th></tr>
		</thead>
		<tbody>
			<?php 
			while($row=mysqli_fetch_array($agree)): 
				?>
				<?php 
					$status_query=mysqli_query($conn,"select * from status where order_id='$row[id]' and repair_id='$repair_id'");
					// if (!$status_query) {
					// 	echo "".mysqli_error($conn);
					// }
				?>
				<tr>
				
					<td><?=$row['name']?></td>
					<td><?=$row['surname']?></td>
					<td><?=$row['father_name']?></td>
					
					<td><?=$row['phone']?></td>
					<?php if(!isset($_GET['q']) || $_GET['q']!=1): ?>
					<td>
		<select name="status_change" class="form-control status_change" id="<?=$row['id']?>" >
						<?php while($row2=mysqli_fetch_array($status_query)): ?>

							<option value="<?=$row2['id']?>" <?=$row2['id']==$row['status_id']?" selected='selected'":''?>><?=$row2['status_name']?></option>
						<?php endwhile ?>
					</select>
					<form action="panel.php" method="get">
						<input type="hidden" name="q" value="1">
						<input type="hidden" name="token" value="<?=$_SESSION['token']?>">
						<button type="sumit" class="btn btn-primary btn-block mt-1">Yeni</button>
					</form>
					</td>
				<?php else: ?>
					<td>
						<form action="" method="post">
							<div class="form-group">
								<input type="hidden" name="order_id"
								value="<?=$row['id']?>">
								<input type="hidden" name="token" value="<?=$_SESSION['token']?>">
								<input required class="form-control" name="new_status" placeholder="Status...">
								<button name="add_new_status" class="btn btn-primary btn-block mt-1">
									Ok
								</button>
							</div>
						</form>
					</td>
				<?php endif ?>

				</tr>
			<?php endwhile; ?>
		</tbody>
	</table>
</div>
<div class="legv_edilenler  tables" style="display: none">

	<table class="table " >
		<h4 class="text-center my-3 ">Legv edilenler</h4>
		<thead>
			<tr><th>Ad</th><th>Soyad</th><th>Atasının Adı</th><th>Telefon</th></tr>
		</thead>
		<tbody>
			<?php 
			while($row=mysqli_fetch_array($disagree)): 
				?>
				
				<tr>
				
					<td><?=$row['name']?></td>
					<td><?=$row['surname']?></td>
					<td><?=$row['father_name']?></td>
					
					<td><?=$row['phone']?></td>
					<td>
						<!-- select -->
					</td>
				</tr>
			<?php endwhile; ?>
		</tbody>
	</table>
	
</div>
<div class="diaqnoz_qoyulmali  tables" style="display: none">
	<table class="table ">
		<h4 class="text-center my-3 ">Diaqnoz qoy</h4>
		<thead>
			<tr><th>Ad</th><th>Soyad</th><th>Marka</th><th>Telefon</th><th>Diaqnoz</th></tr>
		</thead>
		<tbody>
			<?php 
			while($row=mysqli_fetch_array($diaqnoz)): 
				?>
				<?php 
					$d_q=mysqli_query($conn,"select * from diagnose where order_id='$row[id]'");
					if (mysqli_num_rows($d_q)>0) {
						$diagnose=mysqli_fetch_array($d_q);
						$d=$diagnose['name'];
					}
					
				?>
				<tr>
				
					<td><?=$row['name']?></td>
					<td><?=$row['surname']?></td>
					<td><?=$row['device_name']?></td>
					
					<td><?=$row['phone']?></td>
					<td>
<form action="" method="post">

	<div class="form-group">
		<input required type="text" class="form-control" name="diagnose" placeholder="Diaqnoz..." value="<?=(isset($d))?$d:''?>">
		<input type="text" required class="form-control mt-1" name="price" placeholder="Qiymet..." value="<?=(!empty($row['price']))?$row['price']:''?>">
		<input type="hidden" name="token" value="<?=$_SESSION['token']?>">
		<input type="hidden" name="order_id" value="<?=$row['id']?>">
		<button type="submit" class="btn-block btn btn-primary mt-1" name="<?=(isset($d))?'update':'submit'?>">
		<?=(isset($d))?'Update':'Ok'?>
	</button>
	</div>
	
</form>
					</td>
				</tr>
			<?php endwhile; ?>
		</tbody>
	</table>
</div>
					</div>
				</div>	
		</div>
		<div class="col-5" style="height: 100%">
			<img src="../images/Coventry-Mobile-Phone-Repairs-1.png" alt="" class="img-fluid Coventry-Mobile-Phone-Repairs">
		</div>
	</div>
</div>
<script>
	   $('.status_change').change(function(){
           var status=$(this).val();
           var  id=$(this).attr('id');
            $.ajax({
				method:'post',
				url:'partials/_statusChange.php',
				data:{'status':status,'id':id},
				success:function(data){
					$('#'+id).html(data);
				},
				error:function(){alert('something went wrong')}
			});
        });
</script>