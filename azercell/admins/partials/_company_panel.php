<?php 
	if (isset($_SESSION['username'])) {
		$username=$_SESSION['username'];
		$company_query=mysqli_query($conn,"select * from users where username='$_SESSION[username]'");
		$com=mysqli_fetch_array($company_query);
		$c_id=$com['id'];
		

		//orders
		$orders=mysqli_query($conn,"select * from orders where users_id='$c_id'");

		if (isset($_GET['q']) && $_GET['q']==1) {
			if (isset($_POST['submit'])) {
				if (!isset($_POST['token']) || !verifyToken($_POST['token'])) {
		      echo "invalid CSRF! ";die();
		    }
		    else
		    {


				$username=stripper($_POST['username']);
				$password=stripper($_POST['password']);
				$company_id=stripper($c_id);
				$role=3;
				$password=password_hash($password,PASSWORD_DEFAULT);
				$add=mysqli_query($conn,"insert into users(username,password,role) values('$username','$password','$role')");
				
				$id=mysqli_insert_id($conn);
				mysqli_query($conn,"insert into repair(user_id,company_id) values('$id','$company_id')");
				header('location:panel.php');}
			}
		}
	}

	else
		header('location:index.php');



?>

<div class="container-fluid panel-container" >
	<div class="row no-gutters" style="margin-right: -15px;
  margin-left: -15px;height: 100%">
		<div class="col-7">
			<?php if(!isset($_GET['q']) || $_GET['q']!=1): ?>
				<a href="panel.php?q=1" class="btn btn-primary my-2 mx-2" >Temirci əlavə et</a>
				<?php endif ?>
				<div class="row justify-content-center">
					<div class="col-md-11">
						<!-- TABLE -->				
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Ad</th>
						<th>Soyad</th>
						<th>Seriya Nomresi</th>
						<th>Temirci</th>

					</tr>
				</thead>
				<tbody>
					<?php while($row=mysqli_fetch_array($orders)): ?>
						<?php 
						$users=mysqli_query($conn,"Select * from repair where company_id='$c_id'");
						?>
					<tr>
						<td></td>
						<td><?=$row['name']?></td>
						<td><?=$row['surname']?></td>
						<td><?=$row['seria_no']?></td>
	<td>
		<select name="temirci" class="form-control temirci" id="<?=$row['id']?>">
			<?php while($row2=mysqli_fetch_array($users)): ?>
				<?php 
					$getuser=mysqli_query($conn,"select * from users where id='$row2[user_id]'");
					$u=mysqli_fetch_array($getuser);
				?>
				<option value="<?=$row2['id']?>" <?=($row['repair_id']==$row2['id'])?" selected='selected'":''?>><?=$u['username']?></option>
			<?php endwhile ?>
		</select>
	</td>
					</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
					
					</div>
				</div>
			
			<?php if(isset($_SESSION['success'])): ?>
				<div class="row justify-content-center">
					<div class="col-10">
						<div class="alert alert-success">
						<?php 
							echo $_SESSION['success']; 
							unset($_SESSION['success']); 
						?>
						</div>
					</div>
				</div>
			<?php endif ?>
			<?php if(isset($_GET['q']) && $_GET['q']==1): ?>
				<div class="row justify-content-center">
					<div class="col-8 mr-3">
					
				<form action="panel.php?q=1" method="post">
					<div class="form-group">
						<label for="username">Username:</label>
						<input required type="text" name="username" class="form-control">
						<label for="password">Password:</label>
						<input required type="password" class="form-control" name="password">
						<input type="hidden" name="token" value="<?=$_SESSION['token']?>">
					</div>
					<button type="submit" name="submit" class="btn btn-success float-right">Əlavə Et</button>
					<a href="panel.php" class="btn btn-default float-right">Ləğv Et</a>
					
				</form>

					</div>
				</div>
			<?php endif ?>
		</div>
		<div class="col-5" style="height: 100%">
			<img src="../images/Coventry-Mobile-Phone-Repairs-1.png" alt="" class="img-fluid Coventry-Mobile-Phone-Repairs">
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('.panel-container').css({
    			'height':$('body').height()-$('header').height()
    		});

		$('.temirci').change(function(){
			var temirci=$(this).val();
			var id=$(this).attr('id');
			
			$.ajax({
				method:'post',
				url:'partials/_company_cahnge_worker.php',
				data:{'temirci':temirci,'id':id},
				success:function(data){
					$('#'+id).html(data);
				},
				error:function(){alert('something went wrong')}
			});
		});
	});
</script>
<?php include 'partials/_footer.php'; ?>