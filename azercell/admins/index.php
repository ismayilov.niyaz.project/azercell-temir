<?php 
  require_once '../init/core.php';
   include '../_helper.php';
  include 'partials/_head.php';
  include 'partials/_header.php';



  if (isset($_POST['daxil_ol'])) {
    if (!isset($_POST['token']) || !verifyToken($_POST['token'])) {
      echo "invalid CSRF! ";die();
    }
    else
    {
      $username=stripper($_POST['username']);
      $password=stripper($_POST['password']);

    $user_q=mysqli_query($conn,"select * from users where 
      username='$username'");
    if (mysqli_num_rows($user_q)>0) {
      $user=mysqli_fetch_array($user_q);
      if (password_verify($password,$user['password'])) {
        $_SESSION['username']=$user['username'];
        $_SESSION['role']=$user['role'];
        header("location:panel.php");
      }
      else
      {
        $_SESSION['error']='Username ve ya parol duzgun deyl';
      }
    }

      else
      $_SESSION['error']='Username ve ya parol duzgun deyl';
    }
    

  

  }

?>

<div class="container-fluid register-container pt-5 login" >
	<?php if(isset($_SESSION['error'])): ?>
		<div class="alert alert-danger">
			<?=$_SESSION['error']?>
				
			</div>
		<?php unset($_SESSION['error']); ?>
	<?php endif; ?>
	<div class="row justify-content-end" >
		<div class="col-md-6 pt-5">
			<form action="" class="float-right" method="post">
				<div class="form-group">
					<input required type="text" placeholder="Ad..." class="form-elements  form-control" name="username">
					<input required type="password" placeholder="Şifrə..." class="form-elements form-control mt-3" name="password">
          <input type="hidden" name="token" value="<?=$_SESSION['token']?>">
				</div>
				<button type="submit" name="daxil_ol" class="btn btn-block mt-5 py-1 register_btn">DAXIL OL</button>
			</form>
		</div>
	</div>

</div>



<?php include 'partials/_footer.php'; ?>