<?php 
  require_once 'init/core.php';
  include '_helper.php';
  include 'partials/_head.php';
  include 'partials/_header.php';

  if (isset($_SESSION['seria_no'])) {


	$order=mysqli_query($conn,"select * from orders where seria_no='$_SESSION[seria_no]'");

	if (mysqli_num_rows($order)>0) {
			$result=mysqli_fetch_array($order);
			$id=$result['id'];
			//check if queue exist
			$check_query=mysqli_query($conn,"select * from queue where order_id='$id'");

			$diagnose_query=mysqli_query($conn,"select * from diagnose where order_id='$id'");
			$diagnose=mysqli_fetch_array($diagnose_query);
			$status_query=mysqli_query($conn,"select * from status where repair_id='$result[repair_id]' and order_id='$id'");
			if (mysqli_num_rows($status_query)>0) {
				$status_info=mysqli_fetch_array($status_query);
				$status=$status_info['status_name'];
			}
		}
  	

  	if (isset($_POST['novbe_al'])) {
		$date=stripper($_POST['date']);
		$time=stripper($_POST['time']);
		$ticket_numer=rand(1000,9999);
		$check_query=mysqli_query($conn,"select * from queue where order_id='$id'");
		if(mysqli_num_rows($check_query)==0){
			$query=mysqli_query($conn,"insert into queue(order_id,ticket_number,date,time) value('$result[id]',$ticket_numer,'$date','$time')");
			header('location:status.php');
		}
		
	}

	if (isset($_POST['agree'])) {
		mysqli_query($conn,"update orders set repair_status='1', flag_read='1' where id='$id'");
		header('location:status.php');
	}
	if (isset($_POST['disagree'])) {
		mysqli_query($conn,"update orders set repair_status='2'");
		header('location:status.php');
	}
  }

  else
  {
  	header('location:index.php');
  }
?>

<div class="container mt-5">
	<div class="row justify-content-center">
		<div class="col-md-5" >
			<img src="images/iphone-repair-business.png" class="img-fluid" alt="">
		</div>
		<div class="col-md-5">
			<?php if($result['repair_status']==0): ?>
			<h2 class="text-center" style="color:#616161">Diaqnoz</h2>
			<p class="lead">
				<?=$diagnose['name']; ?>
			</p>
			<?php if($result['price']!=0): ?>
			<p class="lead">
				Qiymet: <?=$result['price']?>
			</p>
			<form action="" method="post" class="d-inline">
				<button type="submit" name="agree" class="btn btn-primary">TƏMIR ET</button>
			</form>
			<form action="" method="post" class="d-inline mb-2">
				<button  type="submit" name="disagree" class="btn btn-primary">LƏĞV ET</button>
			</form><br><br>
			<?php endif; ?>
		<?php else: ?>
			<h2 class="text-center">Status</h2>
			<p class="lead">
				<?=$status; ?>
			</p>
		<?php endif; ?>

		
		<!-- must be changed -->
			<?php if($result['status']==0 && $result['price']!=0 && mysqli_num_rows($check_query)==0): ?>
				<button class="btn izle_button" data-toggle="modal" data-target="#exampleModal">Növbə Al</button>
			<?php else: ?>
				<button class="btn izle_button_d" disabled>Növbə Al</button>
			<?php  endif ?>
			<?php if(mysqli_num_rows($check_query)>0): ?>
			<?php $queue=mysqli_fetch_array($check_query); ?>
			
			 <div class="card mt-5">
			    <div class="card-body">
			      <h4 class="text-center card-title">Sizin Növbəniz</h4>
			      <p class="card-text"><?=date('j M',strtotime($queue['date']))?>
				<?=date('H:i',strtotime($queue['time']))?></p>
				<p class="card-text">
			    	<strong>Bilet nömrəsi: <?=$queue['ticket_number']; ?></strong>
			    </p>
			    </div>
			    
			  </div>
			</div>
		<?php endif; ?>
		</div>
	</div>
</div>

<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" method="post">
        	<div class="form-group">
        		<label for="date">Tarix:</label>
        		<input type="date" name="date" class="form-control date">
        	</div>
        	<div class="form-group">
        		<label for="time">Vaxt:</label>
        		<select name="time" class="form-control time">
        			
        		</select>
        	</div>
        	<button type="submit" name="novbe_al"  class="btn izle_button">Novbe al</button>
        </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>

<script>
	$(document).ready(function(){
		$('.date').change(function(){
			var date=$('.date').val();
			$.ajax({
				method:'post',
				url:'partials/_times.php',
				data:{'seria_no':<?=$_SESSION['seria_no']?>,'date':date},
				success:function(data){
					$('.time').html(data);
				},
				error:function(){alert('something went wrong');}
			})
		});
	});
</script>

<?php include 'partials/_footer.php'; ?>