<?php 
	require_once 'init/core.php';
	include '_helper.php';
	include 'partials/_head.php';
	?>
	<meta http-equiv="refresh" content="3" />
	<?php
	include 'partials/_header.php';
	date_default_timezone_set("Asia/Baku");
	$date=date("Y-m-d");
	
	$queue=mysqli_query($conn,"SELECT * FROM `queue` WHERE `queue_status_id`=1 and `date`='$date' order by `time` asc");
	$queue_current=mysqli_query($conn,"SELECT * FROM `queue` WHERE `queue_status_id`=2 and `date`='$date' order by `time` asc");
?>

<div class="container">
	<div class="row justify-content-center">
			
				<div class="col-md-3 mt-2">
					
						<div class="jumbotron jumbotron-fluid">
						  <h1 class="display-5 text-center">
						  	<?php 
						  	$row=mysqli_fetch_array($queue_current);
						  	echo $row['ticket_number'];
						  	?>
						  </h1>
						  <?php if($row['queue_status_id']==2): ?>
						  	<p class="text-danger lead text-center"><strong>Yaxınlaşın</strong></p>
						<?php endif ?>
						</div>
					
				</div>
			
			<div class="w-100"></div>
			<table class="table mt-1 col-md-6">
				<thead>
					<tr>
						<th>#</th>
						<th>Bilet Nömrəsi</th>
						<th>Vaxt</th>
					</tr>
				</thead>
				<tbody>
					<?php while($row=mysqli_fetch_array($queue)): ?>
						<tr>
							<td>	
							</td>
							<td><?=$row['ticket_number']?></td>
							<td><?=$row['time']?></td>
						</tr>
					<?php endwhile ?>
				</tbody>
			</table>
	</div>
</div>

<?php include 'partials/_footer.php'; ?>